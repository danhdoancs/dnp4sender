/* 
 * File:   DoanSelectiveRepeatSender.cpp
 * Author: danh doan
 *
 * Created on November 20, 2015, 9:37 AM
 */

#include <cstdlib>
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading
#include <fcntl.h>
#include <sys/stat.h>

using namespace std;

#define FLAG_DEBUG 1
#define BUFFER_SIZE 1024
#define TIMEOUT 300 //300 mseconds
#define PAYLOAD_SIZE 128

typedef struct {
    unsigned int seq;
    unsigned int ack;
    char payload[PAYLOAD_SIZE + 1];
    int time;
} frame;

int onReceiverMessage(char* message, struct sockaddr_in &client);
int onReceiverUsername(char* body, struct sockaddr_in &client);
int onReceiverPassword(char* body, struct sockaddr_in &client);
int onReceiverInputFile(char* body, struct sockaddr_in &client);
void setupSocket(char* argv[], int &serverSocket, struct sockaddr_in server);
void die(char *s);
char* replace(char *st, char *orig, char *repl);
void clearSession();
void createFrame(char* data);
void removeFrame(char** frameBuffers, int frameIdx);
int checkTimeout(struct sockaddr_in &client);
int onReceiverAck(char* body, struct sockaddr_in &client);
void updateFrameAck(int seq);
void resendFrame(int seq, struct sockaddr_in &client);
void* messageListener(void *);
int checkOldestFrame();
void printStatisticalReport(struct sockaddr_in &client);

//global variables
char cliUsername[BUFFER_SIZE], cliPassword[BUFFER_SIZE];
char buffer[BUFFER_SIZE] = "";
char message[BUFFER_SIZE] = "";
char sendBuffer[BUFFER_SIZE] = "";
char inputFile[BUFFER_SIZE * 10] = "";
char frameBuffer[BUFFER_SIZE] = "";
char fileChunk[PAYLOAD_SIZE] = "";
int serverSocket;
struct sockaddr_in server, client;
int slen = sizeof (server), windowSize;
int currentSeqNo = 0, currentIdx = 0, oldestFrameIdx = 0, flagReadyToSend = 0;
int transmittedPktNo = 0, retransmittedPktNo = 0, receivedAckNo = 0, receivedNeAckNo = 0, latestOkFrameIdx = -1;
frame** frameBuffers;
pthread_mutex_t flagFrameBufferLock;

int main(int argc, char *argv[]) {
    srand(time(NULL));
    //check program parameters
    if (3 != argc) {
        fprintf(stderr, "Usage: %s <port> <windown_size>\n", argv[0]);
        exit(1);
    } else if (atoi(argv[2]) < 1) {
        fprintf(stderr, "Usage: window size must larger than 1\n");
        exit(1);
    }
    windowSize = atoi(argv[2]);
    frameBuffers = (frame**) malloc(windowSize);
    //set up the server socket
    setupSocket(argv, serverSocket, server);

    //create a thread for handle receiver message
    pthread_t thread_id;
    if (pthread_create(&thread_id, NULL, messageListener, NULL) < 0) {
        die("Could not create thread");
    }
    //init lock
    if (pthread_mutex_init(&flagFrameBufferLock, NULL) != 0) {
        printf("\n mutex init failed\n");
        return 1;
    }

    //repeat the procedure for various clients
    while (1) {
        printf("\nWaiting for new client request...\n");
        //wait until ready to send file
        while (flagReadyToSend == 0);
        //read file and check user account
        FILE* fp = fopen(inputFile, "rb");

        do {
            //lock the buffer
            pthread_mutex_lock(&flagFrameBufferLock);
            //check timer
            checkTimeout(client);
            //unlock
            pthread_mutex_unlock(&flagFrameBufferLock);
            //skip if window is full
            if (currentIdx >= windowSize)
                continue;

            memset(sendBuffer, 0, BUFFER_SIZE);
            memset(fileChunk, 0, PAYLOAD_SIZE);
            //start transfering the file
            fread(fileChunk, 1, PAYLOAD_SIZE, fp);
            //send when file is not end
            if (strcmp(fileChunk, "") != 0) {
                createFrame(fileChunk);
                sprintf(sendBuffer, "DATA.%s", frameBuffer);
                if (sendto(serverSocket, sendBuffer, sizeof (sendBuffer), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
                    die("sendto()");
                }
            }
        } while (strcmp(fileChunk, "") != 0);

        //check time out for the current frames
        do {
            //lock the buffer
            pthread_mutex_lock(&flagFrameBufferLock);
            //check timer
            checkTimeout(client);
            //unlock
            pthread_mutex_unlock(&flagFrameBufferLock);
            //skip if window is full
            if (currentIdx >= windowSize)
                continue;
        } while (currentIdx > 0);

        //send end file message
        sprintf(sendBuffer, "FILE.2", buffer);
        if (sendto(serverSocket, sendBuffer, sizeof (sendBuffer), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
            die("sendto()");
        }
        printf("Transfering file %s successfully!\n", inputFile);
        printStatisticalReport(client);
        //clear session
        clearSession();
        //CLOSE FILE
        fclose(fp);
    }

    pthread_join(thread_id, NULL);
    pthread_mutex_destroy(&flagFrameBufferLock);
    return 0;
}

int onReceiverMessage(char* message, struct sockaddr_in &client) {
    char* type = strtok(message, ".");
    char* body = strtok(NULL, "\n");
    //skip if message is wrong format
    if (!type)
        return -1;

    if (strcmp(type, "USERNAME") == 0) {
        return onReceiverUsername(body, client);
    } else if (strcmp(type, "PASSWORD") == 0) {
        return onReceiverPassword(body, client);
    } else if (strcmp(type, "INPUT") == 0) {
        return onReceiverInputFile(body, client);
    } else if (strcmp(type, "ACK") == 0) {
        return onReceiverAck(body, client);
    }
}

int onReceiverAck(char* body, struct sockaddr_in &client) {
    int seqNo = atoi(strtok(body, "."));
    int ack = atoi(strtok(NULL, ""));
    //random drop 10% ACK packets
    double randomProb = (double) rand() / RAND_MAX;
    if (randomProb <= 0.1) {
        printf("ACK of frame %d is dropped!\n", seqNo);
        return 0;
    }

    //received data successful
    //lock the buffer
    pthread_mutex_lock(&flagFrameBufferLock);
    if (ack == 1) { // ack
        printf("Received ACK of frame %d.\n", seqNo);
        updateFrameAck(seqNo);
        receivedAckNo++;
    } else if (ack == -1) { //Negative ack
        printf("Received Negative ACK of frame %d.\n", seqNo);
        resendFrame(seqNo, client);
        receivedNeAckNo++;
    }
    pthread_mutex_unlock(&flagFrameBufferLock);
}

int onReceiverInputFile(char* body, struct sockaddr_in &client) {
    strcpy(inputFile, body);
//    sprintf(inputFile, "./%s", body);
    //check input file
    //read file and check user account
    FILE* fp = fopen(inputFile, "r");

    if (fp == NULL) {
        fprintf(stderr, "Failed to read %s\n", inputFile);
        //send fail read file message
        sprintf(message, "FILE.0");
        if (sendto(serverSocket, message, strlen(message), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
            die("sendto()");
        }
        return 0;
    } else {
        sprintf(message, "FILE.1");
        if (sendto(serverSocket, message, strlen(message), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
            die("sendto()");
        }
        //set flag ready to send data
        flagReadyToSend = 1;
    }
    //CLOSE FILE
    fclose(fp);
    return 1;
}

int onReceiverPassword(char* body, struct sockaddr_in &client) {
    //save password
    strcpy(cliPassword, body);

    //authenticate
    //read file and check user account
    FILE* fp = fopen("./userList.txt", "r");
    char* line = NULL;
    size_t len = 0;
    ssize_t reader;
    int flagExist = 0;

    if (fp == NULL) {
        fprintf(stderr, "Failed to read userList\n");
        return 0;
    } else {
        while ((reader = getline(&line, &len, fp)) != -1) {
            //return successful message if the user is legitemate
            char* usr = strtok(line, " ");
            char* psw = replace(strtok(NULL, " "), "\n", "");
            if (strcmp(usr, cliUsername) == 0 &&
                    strcmp(psw, cliPassword) == 0) {
                flagExist = 1;
                break;
            }
        }
        //user is not exist 
        if (flagExist == 0) {
            //send fail login message
            printf("Client log in failed!\n");
            sprintf(message, "LOGIN.0");
            if (sendto(serverSocket, message, strlen(message), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
                die("sendto()");
            }

            flagExist = 0;
        } else { //user exist
            printf("Client log in successfully!\n");
            sprintf(message, "LOGIN.%d", windowSize);
            //send message for client
            if (sendto(serverSocket, message, strlen(message), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
                die("sendto()");
            }
        }
    }
    //CLOSE FILE
    fclose(fp);
    free(line);
    return flagExist;
}

int onReceiverUsername(char* body, struct sockaddr_in &client) {
    strcpy(cliUsername, body);
}

void setupSocket(char* argv[], int &serverSocket, struct sockaddr_in server) {
    //Create UDP socket
    serverSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    int flags = fcntl(serverSocket, F_GETFL);
    flags |= O_NONBLOCK;
    fcntl(serverSocket, F_SETFL, flags);

    if (serverSocket == -1) {
        die("Could not create server socket");
    }
    puts("Server socket created");

    //Prepare the sockaddr_in structure
    memset((char *) &server, 0, sizeof (server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(atoi(argv[1]));

    //Bind
    if (bind(serverSocket, (struct sockaddr *) &server, sizeof (server)) < 0) {
        //print the error message
        die("Bind failed. Error");
    }
    puts("Bind done");
}

void die(char *s) {
    perror(s);
    exit(1);
}

void debug(char* str) {
    if (FLAG_DEBUG)
        fprintf(stderr, "Debug: %s\n", str);
}

char* replace(char *st, char *orig, char *repl) {
    static char buffer[BUFFER_SIZE];
    char *ch;
    if (!(ch = strstr(st, orig)))
        return st;
    strncpy(buffer, st, ch - st);
    buffer[ch - st] = 0;
    sprintf(buffer + (ch - st), "%s%s", repl, ch + strlen(orig));
    return buffer;
}

void clearSession() {
    memset(cliUsername, 0, BUFFER_SIZE);
    memset(cliPassword, 0, BUFFER_SIZE);
    memset(buffer, 0, BUFFER_SIZE);
    memset(sendBuffer, 0, BUFFER_SIZE);
    memset(message, 0, BUFFER_SIZE);
    flagReadyToSend = 0;
    currentIdx = 0;
    currentSeqNo = 0;
    transmittedPktNo = 0;
    retransmittedPktNo = 0;
    receivedAckNo = 0;
    receivedNeAckNo = 0;
}

void createFrame(char* data) {
    memset(frameBuffer, 0, BUFFER_SIZE);
    sprintf(frameBuffer, "%d.%s", currentSeqNo, data);
    frame *pkt = new frame();
    strcpy(pkt->payload, data);
    pkt->seq = currentSeqNo++;
    pkt->ack = 0;
    pkt->time = clock();
    frameBuffers[currentIdx] = pkt;
    currentIdx = currentIdx + 1;
    transmittedPktNo++;
    
    char subData[PAYLOAD_SIZE+1];
    memcpy(subData, data, PAYLOAD_SIZE+1);
    printf("Frame %d is sent: %s!\n", pkt->seq, strtok(subData, "\003"));
}

void resendFrame(int seq, struct sockaddr_in &client) {
    //find the idx
    int frameIdx = -1;
    for (int i = 0; i < currentIdx; i++) {
        if (frameBuffers[i]->seq == seq) {
            frameIdx = i;
            break;
        }
    }
    if (frameIdx == -1) { //not found, drop
        //        printf("Frame %d not found, skip resending\n", seq);
        return;
    }

    frameBuffers[frameIdx]->ack = -1;
    //resend frame
    printf("Frame %d is lost, resending!\n", frameBuffers[frameIdx]->seq);
    sprintf(message, "DATA.%d.%s", frameBuffers[frameIdx]->seq, frameBuffers[frameIdx]->payload);
    if (sendto(serverSocket, message, sizeof (message), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
        die("sendto()");
    }
    retransmittedPktNo++;
}

//remove frame from list

void updateFrameAck(int seq) {
    //find the idx
    int frameIdx = -1;
    for (int i = 0; i < currentIdx; i++) {
        if (frameBuffers[i]->seq == seq) {
            frameIdx = i;
            break;
        }
    }
    if (frameIdx == -1) { //not found, drop
        //        printf("Received frame %d not found in list\n!", seq);
        return;
    }
    //update ok status for all frames that up to received ack frame 
    if (latestOkFrameIdx < frameIdx) {
        for (int i = latestOkFrameIdx + 1; i <= frameIdx; i++) {
            if (frameBuffers[i]->ack != 1) {
                frameBuffers[i]->ack = 1;
                printf("Frame %d is received successfully!\n", frameBuffers[i]->seq);
            }
        }
        //update the latestOKFrame
        latestOkFrameIdx = frameIdx;
        //delete if it is the oldest frame
        if (frameIdx == 0) {
            //delete the frame
//            free(frameBuffers[frameIdx]);
            //            printf("Removed frame %d out of window\n", seq);
            //if there is only one frame, just overwrite that frame
            if (frameIdx == currentIdx - 1) {
                //update current available Idx
                currentIdx--;
            } else {
                for (int i = frameIdx; i < currentIdx - 1; i++) {
                    frameBuffers[i] = frameBuffers[i + 1];
                }
                //update current available Idx
                currentIdx--;
            }
            latestOkFrameIdx--;
        } else { //not the oldest frame, update ack only
            frameBuffers[frameIdx]->ack = 1;
        }
        //check other frames
        checkOldestFrame();
    } else { // receiver is waiting for a lost frame

    }
}

int checkTimeout(struct sockaddr_in &client) {
    int now = clock();
    for (int i = 0; i < currentIdx; i++) {
        //time out
        if (((now - frameBuffers[i]->time) * 1000.0 / CLOCKS_PER_SEC) >= TIMEOUT && frameBuffers[i]->ack != 1) {
            memset(sendBuffer, 0, BUFFER_SIZE);
            //resend frame
            sprintf(sendBuffer, "DATA.%d.%s", frameBuffers[i]->seq, frameBuffers[i]->payload);
            if (sendto(serverSocket, sendBuffer, sizeof (sendBuffer), 0, (struct sockaddr *) &client, sizeof (client)) == -1) {
                die("sendto()");
            }
            printf("Frame %d got timeout, resending!\n", frameBuffers[i]->seq);
            //update time
            frameBuffers[i]->time = now;
            retransmittedPktNo++;
        }
    }
}

int checkOldestFrame() {
    int frameIdx = 0;
    //delete if it is the oldest frame and ack == 1
    if (currentIdx > 0 && frameBuffers[frameIdx]->ack == 1) {
        //        printf("Clear oldest received frame %d\n", frameBuffers[frameIdx]->seq);
        //delete the frame
//        free(frameBuffers[frameIdx]);
        //if there is only one frame, just overwrite that frame
        if (frameIdx == currentIdx - 1) {
            //update current available Idx
            currentIdx--;
        } else {
            for (int i = frameIdx; i < currentIdx - 1; i++) {
                frameBuffers[i] = frameBuffers[i + 1];
            }
            //update current available Idx
            currentIdx--;
        }
        latestOkFrameIdx--;
        //recall to check next frame
        return checkOldestFrame();
    }
}

void* messageListener(void *) {
    //Get the socket descriptor
    //    struct sockaddr_in server;
    //loop to waiting for message from server
    int recvLen, status;
    while (1) {
        memset(buffer, 0, BUFFER_SIZE);
        //memset the buffer
        memset(message, 0, BUFFER_SIZE);
        socklen_t clientLen = sizeof (client);
        if ((recvLen = recvfrom(serverSocket, buffer, BUFFER_SIZE, 0, (struct sockaddr *) &client,
                &clientLen)) > 0) {
            status = onReceiverMessage(buffer, client);
            if (status < 0) {
                exit(1);
            }
        }
    }
    return 0;
}

void printStatisticalReport(struct sockaddr_in &client) {
    int fildes = open(inputFile, O_RDWR);
    struct stat statBuffer;
    fstat(fildes, &statBuffer);
    //get file size
    int fileSize = statBuffer.st_size;
    //get file creation date
    char creationDateStr[BUFFER_SIZE];
    struct tm* clock;
    clock = gmtime(&(statBuffer.st_mtime));
    sprintf(creationDateStr, "%d-%d-%d %d:%d:%d", clock->tm_year + 1900, clock->tm_mon + 1, clock->tm_mday, clock->tm_hour, clock->tm_min, clock->tm_sec);

    printf("Statistical Report:\n");
    printf("Receiver: %s:%d\n", inet_ntoa(client.sin_addr), (int) ntohs(client.sin_port));
    printf("Input file: %s (%d bytes)\n", inputFile, fileSize);
    printf("File created: %s\n", creationDateStr);
    printf("Number of packets transmitted: %d\n", transmittedPktNo);
    printf("Number of packets re-transmitted: %d\n", retransmittedPktNo);
    printf("Number of ACK packets received: %d\n", receivedAckNo);
    printf("Number of Negative ACK pakcets recevied: %d\n", receivedNeAckNo);
}